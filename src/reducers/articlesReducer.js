const articlesReducerDefaultState = {};

export default (state = articlesReducerDefaultState, action) => {
    switch (action.type){
        case 'SET_ARTICLES':
            return {
                ...state,
                articles: action.articles
            };
        case 'REMOVE_ARTICLES':
            return {
                ...state,
                articles: undefined
            };
        default:
            return state;
    }
};
