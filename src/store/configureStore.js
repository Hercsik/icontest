import {createStore, combineReducers, compose } from 'redux';
import articlesReducer from '../reducers/articlesReducer';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {

    const store = createStore(
        combineReducers({
            articles: articlesReducer
        }),
        composeEnhancers()
    );

    return store
};