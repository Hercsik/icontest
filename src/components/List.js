import React, {Component} from 'react';
import ListItem from './ListItem';
import {connect} from 'react-redux';
import style from '../styles/List.css';

class List extends Component {
    
    constructor(props){
        super(props);
        this.state = {};
    }
    
    onClear = (e) => {
        e.preventDefault();
        this.props.dispatch({type: 'REMOVE_ARTICLES'})
    }
    
    
    render(){
        return(
            <div>
                {!!this.props.feed.articles ?
                 
                 this.props.feed.articles.map((data) =>
                 {
                        return <ListItem  key={data.id['_text']} {...data} />
                 }) : <p className={style.no_articles}>Nincsenek cikkek</p>
                }
                {!!this.props.feed.articles ?
                    <button className={style.clear_button} onClick={this.onClear} >Cikkek ürítése</button> : ''
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        feed: state.articles
    };
};

export default connect(mapStateToProps)(List);