import React, {Component} from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import xml from 'xml-js';
import style from '../styles/Form.css';

class Form extends Component {
    constructor(props){
        super(props);
        this.state = {
            url: '',
            error: ''
        };
        this.errors = ['Atom feed linket adj meg kérlek!',
                       'A feedben nem található bejegyzés!',
                       'Hálózati hiba történt, kérjük próbálja meg később!',
                       'Valami hiba történt!',
                       'Adj meg egy érvényes Atom feed URL-t, kérlek!'
                      ];
    }
    
    

    onURLChange = (e) => {
        const url = e.target.value;
        this.setState(() => ({
            url
        }));
    };
    
    onSubmit = (e) => {
        e.preventDefault();
        const url = this.state.url;
        
        if(!url){
            this.setState(() => ({
                error: this.errors[4]
            }));
            return 0;
        }
        
        this.setState(() => ({
            url: '',
            error: ''
        }));
        
        const converter = xml;
        
//        let config = {
//            method: 'get',
//            url: url,
//            crossDomain: true,
//        }
        const backend = ['https://thing123iw.tk/api.php', 'https://api.senwar.org/atom'];
        
        axios.post( backend[1] ,{url})
            .then(res => {
                //        request        console.log(res);
            
                // https://github.com/CodeMangler.atom
            
                //Checking content-type
                if(!res.headers['content-type'].includes('atom')){
                    throw this.errors[0];
                }
                //Converting XML to JSON and preparing
                let articles = undefined;
                let json = converter.xml2json(res.data, {
                    compact: true,
                    spaces: 4
                });
                json = JSON.parse(json);
                articles = json.feed.entry;
                // Checking if the feed contains any entry
                if(!articles){
                    throw this.errors[1];
                }
                return articles;
            })
            .then(res => {
                //Redux
                this.props.dispatch({
                    type: 'SET_ARTICLES',
                    articles: res
                });
            })
            .catch(err => {
            if(err.toString().match(this.errors[0]) || 
               err.toString().match(this.errors[1])){
                this.setState(() =>({
                    error: err
                }));
            } else if(err.toString().includes('Network Error')) {
                        this.setState(() =>({
                    error: this.errors[2]
                }));
                      } else {
                this.setState(() =>({
                    error: this.errors[3]
                }));
            }
                
            });




        //            const url = 'https://stackoverflow.com/feeds/question/10943544';
        //            const url1 = 'https://github.com/CodeMangler.atom';
        //            const url1_2 = 'https://github.com/thing123.atom';
        //            const url2 = 'http://www.animerush.tv/rss.xml';
        //            const url3 = 'https://jsonplaceholder.typicode.com/posts';
        //            const url4 = 'https://www.npmjs.com/package/axios';
        //   nagy nodeszámnál range error maximum call stack size exceeded, setTimeout nem //   oldja meg, testData-nál jól futott
        //        
        //        const getObjects = (obj, key) => {
        //
        //            let result = [];
        //            
        //            if (obj instanceof Array) {
        //                obj.forEach((item, index) => {
        //                    getObjects(item).forEach((article) => {
        //                        result.push(article);
        //                    });
        //                });
        //            } else {
        //                for (let prop in obj) {
        //                    if (prop === 'entry') {
        //                        obj[prop].forEach((article) => {
        //                            result.push(article);
        //                        });
        //                    } else {
        //                        getObjects(obj[prop]).forEach((article) => {
        //                            result.push(article);
        //                        });
        //                    }
        //                }
        //            }
        //            return result;
        //        }

        //        let entries = undefined;
        //        entries = getObjects(testData, 'entry');
        //        console.log(entries);


    }

    render(){
        return(
            <div className={style.wrapper}>
                <form onSubmit={this.onSubmit} >
                    <input 
                        className={style.input}
                        type='text' 
                        placeholder='URL...'
                        autoFocus
                        value={this.state.url}
                        onChange={this.onURLChange}
                    />
                    <button className={style.submit} >Submit</button>
                </form>
                {!!this.state.error ? <p className={style.error}>{this.state.error}</p> : <p></p> }
            </div>
        );
    }
}

export default connect()(Form);