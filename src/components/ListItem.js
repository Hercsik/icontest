import React from 'react';
import style from '../styles/Article.css';

const ListItem = (props) => (
    <div className={style.article}>
        <p className={style.title}>{props.title['_text']}</p>
        <div className={style.date_container}>
            <p className={style.date}>{props.published['_text'].split(/([0-9]{4}[-][0-1][0-9][-][0-3][0-9])/)[1]}</p>
            <p className={style.clock} >{props.published['_text'].split(/([0-2][0-9][:][0-5][0-9][:][0-5][0-9])/)[1]}</p>
        </div>
        <p className={style.author}><span className={style.author_span}>Szerző:</span> {props.author.name['_text']}</p>
    
        {!!props.summary ? <p className={style.summary}>{props.summary['_text']}</p> : <p className={style.summary} >Nincs megadott összefoglaló</p>}
    
        {(!!props.link['_attributes'].href) ? 
        <a 
            className={style.to_source}
            href={props.link['_attributes'].href}
            target='_blank'
        >Tovább...</a> 
        : 
        <p>Nincs publikus elérési útvonal</p>}
    
        
    </div>
);

export default ListItem;


//        <p>{props.title['_text']}</p>
//        <p>{props.author.name}</p>

//        {!!props.link.attributes.href ? <a 
//            href={props.link.attributes.href}
//        >Tovább</a> : <p></p>}

//        {!!props.summary ? <p>props.summary.text</p> : <p></p>}