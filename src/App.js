import React, { Component } from 'react';
import Form from './components/Form';
import List from './components/List';
import style from './styles/App.css';



class App extends Component {
    constructor(props){
        super(props);
        this.state = {};
    }
  render() {
    return (
      <div className={style.app}>
        <h1 className={style.title} >Feed me with atom URL</h1>
        <Form />
        <List />
      </div>
    );
  }
}

export default App;